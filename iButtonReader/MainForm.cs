﻿using System;
using System.Runtime.InteropServices;
using com.dalsemi.onewire.adapter;
using com.dalsemi.onewire.container;
using com.dalsemi.onewire;
using System.Threading;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;
using System.Media;

namespace iButtonReader
{
    public partial class MainForm : System.Windows.Forms.Form
    {        
        [DllImport("kernel32", SetLastError = true)]
        static extern IntPtr LoadLibrary(string lpFileName);
        static DSPortAdapter adapter { get; set; }
        public static string Status = "";
        string ShablonText = "код брелока";
        private string CurrentDirectopy = Directory.GetCurrentDirectory();
        public MainForm()
        {
            InitializeComponent();
            Visible = false;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            var Files = Directory.GetFiles(CurrentDirectopy);
            if (Files.Length > 0)
            {
                foreach (var file in Files)
                {
                    var folders = file.Split('\\');
                    var NewPath = folders[folders.Length - 1];
                    if (folders[folders.Length - 1].ToLower().Contains("hide"))
                    {
                        Visible = false;
                    }
                }
            }

            if (Environment.Version.Major >= 4)
            {
                string folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), @"..\Microsoft.NET\Framework\v2.0.50727");
                folder = Path.GetFullPath(folder);
                LoadLibrary(Path.Combine(folder, "vjsnativ.dll"));
            }
            try
            {
                CheckShablon();
                adapter = OneWireAccessProvider.getDefaultAdapter();
                StatusField.Text = "Считыватель обнаружен: " + adapter.ToString();                
                new Thread(CheckNumber).Start(this);
            }
            catch
            {
                StatusField.Text = "Считыватель не обнаружен!";
                NumberBox.Enabled = false;                
                return;
            }            
        }

        private void CheckShablon()
        {
            string text = null;
            try
            {
                text = File.ReadAllText(CurrentDirectopy + @"\shablon.txt");
            } catch { }
            
            if (text != null)
            {
                try
                {
                    Shablon.Text = text;
                }
                catch { Shablon.Text = "код брелока"; }
            }
            else
                Shablon.Text = "код брелока";            
        }

        private void CheckNumber(object MainF)
        {
            var Main = MainF as MainForm;
            while (true)
            {
                Thread.Sleep(500);
                if (adapter != null)
                {
                    try
                    {
                        adapter.beginExclusive(true);
                        adapter.setSearchAllDevices();
                        adapter.targetAllFamilies();
                        adapter.setSpeed(0);
                        try
                        {
                            var AdCont = adapter.getAllDeviceContainers();
                            if (AdCont.hasMoreElements())
                            {
                                StatusField.Text = "ОК";
                                var NextContainer = (OneWireContainer)AdCont.nextElement();
                                var CodInt = NextContainer.getAddressAsLong();
                                var CodeStr = NextContainer.getAddressAsString();
                                if (CodeStr != Status)
                                {                                    
                                    Status = CodeStr;
                                    try
                                    {
                                        SoundPlayer Sound = new SoundPlayer(Directory.GetCurrentDirectory() + "\\Beep.wav");
                                        Sound.Play();
                                    }
                                    catch { }

                                    try
                                    {
                                        Main.Invoke(new System.Action(() => Clipboard.SetText(CodeStr)));
                                        Thread.Sleep(100);
                                        switch(ShablonText)
                                        {
                                            case "код брелока": Main.Invoke(new System.Action(() => SendKeys.Send("+{INS}")));
                                                break;
                                            case "код брелока+Enter": Main.Invoke(new System.Action(() => SendKeys.Send("+{INS}{ENTER}")));
                                                break;
                                            case "Enter + код брелока + Enter":
                                                Main.Invoke(new System.Action(() => SendKeys.Send("{ENTER}+{INS}{ENTER}")));
                                                break;
                                            case "Enter + код брелока + Enter + \"Стрелка вниз\"": Main.Invoke(new System.Action(() => SendKeys.Send("{ENTER}+{INS}{ENTER}{DOWN}")));
                                                break;
                                            default: Main.Invoke(new System.Action(() => SendKeys.Send("+{INS}" + Environment.NewLine)));
                                                break;
                                        }
                                        NumberBox.Text = CodeStr;
                                        //Main.Invoke(new System.Action(() => SendKeys.SendWait("+{INS}" + Environment.NewLine)));
                                    }
                                    catch { }
                                }
                            }
                            else
                            {
                                Status = "";
                                try
                                {
                                    StatusField.Text = "Отсутствует брелок в считывателе!";
                                    NumberBox.Text = "";
                                } catch { }
                            }

                            
                        }
                        catch { }
                        adapter.endExclusive();
                    }
                    catch 
                    {                        
                        adapter.endExclusive();
                    }
                }
                else { try { StatusField.Text = "Устройтво не обнаружено!"; } catch { } }
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }

        private void MainForm_VisibleChanged(object sender, EventArgs e)
        {
            var Files = Directory.GetFiles(CurrentDirectopy);
            if (Files.Length > 0)
            {
                foreach (var file in Files)
                {
                    var folders = file.Split('\\');
                    var NewPath = folders[folders.Length - 1];
                    if (folders[folders.Length - 1].ToLower().Contains("hide"))
                    {
                        Visible = false;
                    }
                }
            }            
        }

        private void Shablon_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShablonText = Shablon.Text;
            File.WriteAllText(CurrentDirectopy + @"\shablon.txt", ShablonText);
        }
    }
}
